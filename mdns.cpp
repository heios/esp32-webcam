#include <ESPmDNS.h>
namespace dns {
  
bool AdvertiseServices(const char *MyName)
{
  if (false == MDNS.begin(MyName)) {
    return false;
  }

  // Add service to MDNS-SD
  MDNS.addService("http", "tcp", 80);
}

}  // namespace dns
