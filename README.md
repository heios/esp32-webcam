# Esp32 Webcam

Firmware for ESP32 webcam with mDNS support and name generator.


## How to upload.

1. Connect arduino to AI Thinker ESP32-CAM: pin 0 to UnR, pin 1 to UOT.
2. Connect IOD of ESP32-CAM to ground.
3. In arduino IDE, tools:
   1. Board "AI Thinker ESP32-CAM" or "ESP32 Wrover Module".
   2. Upload speed: 115200.
   3. Flash frequency: 40MHz.
   4. Flash mode: QIO.
   5. Partition scheme: Huge APP (3MB No OTA/1MB SPIFFS).
   6. Core Debug Level: None.
   7. Select current port.
   8. Programmer: I had none for this board and everything was fine.
   
4. Copy `WIFI_CONFIG.h.template` to `WIFI_CONFIG.h`, and set SSID and password for wifi access point.
5. Compile, upload, checkout health over serial monitor (on 115200 baud speed).

## Camera discovery in local network

### Linux

`avahi-browse -a`

or

`avahi-browse -a | grep cam_esp32`
