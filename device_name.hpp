#include <cstring>
#include <algorithm>
#include <ArduinoUniqueID.h>
#include "totro.hpp"


namespace device_name {

uint32_t inline MurmurOAAT32 (const uint8_t* key, size_t len)
{
  uint32_t h(3323198485ul);
  for (size_t i = 0; i < len; ++i) {
    h ^= key[i];
    h *= 0x5bd1e995;
    h ^= h >> 15;
  }
  return h;
}

std::array<char, 16> GenDeviceName(const char* prefix) {
  auto result = std::array<char, 16>{};
  constexpr auto result_max_len = result.size() - sizeof('\0');
  const auto prefix_useful_len = std::min(strlen(prefix), result_max_len);

  std::copy(prefix, std::next(prefix, prefix_useful_len), result.data());

  constexpr auto appendium_desired_len = 5u;
  const auto appendium_len =
    std::min(
      appendium_desired_len,
      result_max_len - prefix_useful_len);
  const auto seed = MurmurOAAT32(UniqueID, UniqueIDsize);

  totro::Generate(
    std::next(result.data(), prefix_useful_len),
    appendium_len,
    seed);

  return result;
}

}  // namespace device_name 
